
.PHONY:
init: up wait mysql-restore

.PHONY:
reload: down build

.PHONY:
shell:
	docker exec -it django-app bash

.PHONY:
mysql-dump:
	docker exec django-mysql /usr/bin/mysqldump --no-tablespaces -u test --password=test django_db > ./fixtures/django_db.sql

.PHONY:
mysql-restore:
	cat ./fixtures/django_db.sql | docker exec -i django-mysql /usr/bin/mysql -u test --password=test django_db

# More targets

.PHONY:
up:
	docker-compose up -d

.PHONY:
restart:
	docker-compose restart

.PHONY:
down:
	docker-compose down

.PHONY:
build:
	docker-compose build

.PHONY:
wait:
	sleep 10
