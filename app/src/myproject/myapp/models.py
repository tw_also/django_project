from django.db import models


class City(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Mark(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Advertisement(models.Model):
    title = models.CharField(max_length=100)
    image = models.URLField(max_length=255)
    year = models.IntegerField(default=0)
    price = models.IntegerField(default=0)
    description = models.TextField()
    mark = models.ForeignKey(Mark, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.id}. {self.title} {self.price}"
