from django.shortcuts import render

from .models import Advertisement, City, Mark


def index(request):
    city_id = request.GET.get('city', None)
    mark_id = request.GET.get('mark', None)

    marks = Mark.objects.all()
    cities = City.objects.all()

    advertisements = Advertisement.objects.all()
    if city_id:
        advertisements = advertisements.filter(city=city_id)
    if mark_id:
        advertisements = advertisements.filter(mark=mark_id)

    return render(request, 'myapp/index.html', {
        'marks': marks,
        'cities': cities,
        'advertisements': advertisements,
    })


def details(request):
    return render(request, 'myapp/details.html')
