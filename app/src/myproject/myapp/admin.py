from django.contrib import admin

from .models import City, Mark, Advertisement

admin.site.register(City)
admin.site.register(Mark)
admin.site.register(Advertisement)
