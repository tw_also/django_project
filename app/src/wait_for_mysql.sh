# Sometimes Django starts before the mysql is loaded.
# To avoid this, we are waiting x seconds before launch Django.
sleep 10

/usr/local/bin/python /app/myproject/manage.py runserver 0.0.0.0:8080
